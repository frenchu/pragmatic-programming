+++
author = "Paweł Weselak"
title = "Kotlintest adventures"
description = "Common kotlintest pitfalls and how to deal with them"
date = 2020-03-14T22:45:41+02:00
publishdate = 2020-03-14
tags = [
    "kotlintest",
    "kotlin",
    "jvm"
]
categories = [
    "development",
    "qa"
]
series = ["Kotlintest adoption"]
aliases = ["kotlintest-adventures"]
+++

Common pitfalls and how to deal with them

## Intro
